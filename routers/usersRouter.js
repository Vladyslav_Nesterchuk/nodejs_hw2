const express = require('express');
const router = new express.Router();
const {validatePasswordChange} = require('./middlewares/validationMiddeleware');
const {asyncWrapper} = require('./helpers');
const {
  getProfile,
  changePassword,
  deleteProfile,
} = require('../controlers/usersController');

router.route('/me')
    .get(asyncWrapper(getProfile))
    .patch(
        asyncWrapper(validatePasswordChange),
        asyncWrapper(changePassword))
    .delete(asyncWrapper(deleteProfile));

module.exports = router;
