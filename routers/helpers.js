module.exports.asyncWrapper = function(callback) {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
};

module.exports.errorMapper = function(error) {
  switch (error.name) {
    case 'AuthHeaderError':
    case 'DataValidationEroor':
    case 'PasswordError':
    case 'TokenError':
    case 'ObjectExistenceError':
    case 'InputValidationError':
      return {staus: 400, message: error.message};
    default:
      return {staus: 500, message: 'Internal server error'};
  };
};
