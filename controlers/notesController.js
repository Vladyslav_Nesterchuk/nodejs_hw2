const noteService = require('../services/noteService');

module.exports.getAllNotes = async (req, res) => {
  limit = +req.body.limit || 5;
  skip = +req.body.offset || 0;
  if (limit > 30) {
    limit = 30;
  };
  const notes = await noteService.getAllUsersNote( req.user._id, limit, skip);
  res.json({notes: notes});
};

module.exports.newNote = async (req, res) => {
  noteService.saveNewNote(req.user._id, req.body.text);
  res.json({'message': `Success`});
};

module.exports.getNoteById = async (req, res) => {
  const note = await noteService.getNoteById(req.params.id, req.user._id);
  res.json({'note': note});
};

module.exports.updateNoteById = async (req, res) => {
  await noteService.updateNoteById(req.params.id, req.user._id, req.body.text);
  res.json({'message': `Success`});
};

module.exports.changeStatus = async (req, res) => {
  await noteService.changeStatus(req.params.id, req.user._id);
  res.json({'message': `Success`});
};

module.exports.deleteNoteById = async (req, res) => {
  await noteService.deleteNoteById(req.params.id, req.user._id);
  res.json({'message': `Success`});
};
