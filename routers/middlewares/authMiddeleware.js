const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');
const userServise = require('../../services/userService');
const TokenError = require('../../customErrors/TokenError');
const AuthHederError = require('../../customErrors/AuthHederError');

module.exports = async (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    throw new AuthHederError(`No Authorization http header found!`);
  };
  const token = header.split(' ')[1];
  if (!token) {
    throw new AuthHederError(`No JWT token found!`);
  };
  try {
    req.user = jwt.verify(token, JWT_SECRET);
    await userServise.getProfile(req.user._id);
  } catch (err) {
    throw new TokenError('No valid token');
  }
  next();
};
