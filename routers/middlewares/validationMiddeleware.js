const Joi = require('joi');
const passwordRegExp = new RegExp('^[a-zA-Z0-9]{6,30}$');
const objectIDRegExp = new RegExp('^[0-9a-fA-F]{24}$');
const InputValidationError = require('../../customErrors/InputValidationError');
const registrationSchema = {
  username: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required()
      .error(new InputValidationError('Username no valid')),
  password: Joi.string()
      .pattern(passwordRegExp)
      .error(new InputValidationError('Password no valid')),
};

const noteDataSchema = {
  text: Joi.string()
      .required()
      .error(new InputValidationError('Note text no valid')),
};

const passwordChangeShema = {
  newPassword: Joi.string()
      .required()
      .pattern(passwordRegExp)
      .error(new InputValidationError('New password no valid')),
  oldPassword: Joi.string()
      .required()
      .pattern(passwordRegExp)
      .error(new InputValidationError('Old password no valid')),
};

const idShema = {
  id: Joi.string()
      .pattern(objectIDRegExp)
      .error(new InputValidationError('ID no valid')),
};

/**
 * @param {Object} schemaObj
 * @param {Object} data
 */
async function validate(schemaObj, data) {
  await Joi.object(schemaObj).validateAsync(data);
}

module.exports.validateRegistration = async (req, res, next) => {
  await validate(registrationSchema, req.body);
  next();
};

module.exports.validateNoteData = async (req, res, next) => {
  await validate(noteDataSchema, req.body);
  next();
};

module.exports.validatePasswordChange = async (req, res, next) => {
  await validate(passwordChangeShema, req.body);
  next();
};

module.exports.validateId = async (req, res, next) => {
  await validate(idShema, req.params);
  next();
};
