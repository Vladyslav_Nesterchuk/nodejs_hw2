const express = require('express');
const router = new express.Router();
const {
  validateNoteData,
  validateId} = require('./middlewares/validationMiddeleware');
const {asyncWrapper} = require('./helpers');
const {
  getAllNotes,
  newNote,
  getNoteById,
  updateNoteById,
  changeStatus,
  deleteNoteById,
} = require('../controlers/notesController');

router.route('/')
    .get(asyncWrapper(getAllNotes))
    .post(asyncWrapper(validateNoteData), asyncWrapper(newNote));

router.route('/:id')
    .all(asyncWrapper(validateId))
    .get(asyncWrapper(getNoteById))
    .patch(asyncWrapper(changeStatus))
    .delete(asyncWrapper(deleteNoteById))
    .put(asyncWrapper(validateNoteData),
        asyncWrapper(updateNoteById));

module.exports = router;
