const {Note} = require('../models/noteModel');
const ObjectExistenceError = require('../customErrors/ObjectExistenceError');

module.exports.getAllUsersNote = async (userId, limit, skip) => {
  return notes = await Note.find(
      {userId},
      {'__v': 0},
      {limit: limit, skip: skip},
  );
};

module.exports.getNoteById = async (noteId, userId) => {
  const note = await Note.findOne({
    '_id': noteId,
    'userId': userId},
  {'__v': 0},
  );
  if (!note) {
    throw new ObjectExistenceError(`No note with id: ${noteId}`);
  };
  return note;
};

module.exports.saveNewNote = async (userId, noteText) => {
  const notes = new Note({
    'userId': userId,
    'text': noteText,
  });
  await notes.save();
};

module.exports.updateNoteById = async (noteId, userId, newText) => {
  const note = await Note.findOneAndUpdate({
    '_id': noteId,
    'userId': userId,
  }, {
    'text': newText,
  });
  if (!note) {
    throw new ObjectExistenceError(`No note with id: ${noteId}`);
  };
};

module.exports.deleteNoteById = async (noteId, userId) => {
  const note = await Note.findOneAndDelete({
    '_id': noteId,
    'userId': userId,
  });
  if (!note) {
    throw new ObjectExistenceError(`No note with id: ${userId}`);
  };
};

module.exports.changeStatus = async (noteId, userId) => {
  const note = await this.getNoteById(noteId, userId);
  return await Note.findOneAndUpdate({
    '_id': noteId,
  }, {
    'completed': !note.completed,
  });
};
