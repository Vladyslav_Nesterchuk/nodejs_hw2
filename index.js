const express = require('express');
const morgan = require('morgan');
const config = require('./config');
const dbConnect = require('./dbConnect');
const {errorMapper} = require('./routers/helpers')

const app = express();

const {asyncWrapper} = require('./routers/helpers');
const authMiddleware = require('./routers/middlewares/authMiddeleware');

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', asyncWrapper(authMiddleware), usersRouter);
app.use('/api/notes', asyncWrapper(authMiddleware), notesRouter);
app.use('/',(req, res) => {
  res.status(404).json({'message': 'Page not found'})
} )

app.use((err, req, res, next) => {
  console.log(err);
  const errorRes = errorMapper(err);
  res.status(errorRes.staus).json({'message': errorRes.message});
});

const start = async () => {
  await dbConnect.mongooseConnect();
  app.listen(config.PORT, () => {
    console.log(`Server works at port ${config.PORT}`);
  });
};

start();
