const {JWT_SECRET} = require('../config');
const jwt = require('jsonwebtoken');
const userServise = require('../services/userService');

module.exports.registration = async (req, res) => {
  await userServise.saveNewUser(req.body.username, req.body.password);
  res.json({message: `User created successfuly!`});
};

module.exports.login = async (req, res) => {
  const user = await userServise.getProfile(null, req.body.username);
  await userServise.passwordCompare(user._id, req.body.password);
  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);
  res.json({'message': 'Success', 'jwt_token': token});
};
