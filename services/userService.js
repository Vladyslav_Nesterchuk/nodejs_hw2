const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');
const bcrypt = require('bcrypt');
const PasswordErrors = require('../customErrors/PasswordError');
const ObjectExistenceError = require('../customErrors/ObjectExistenceError');

module.exports.saveNewUser = async (username, password) => {
  const user = new User({
    username,
    'password': await bcrypt.hash(password, 10),
  });
  await user.save();
};

module.exports.getProfile = async (userId, username) => {
  const user = userId ? await User.findById(userId):
                        await User.findOne({'username': username});
  if (!user) {
    throw new ObjectExistenceError(`User not found`);
  };
  return user;
};

module.exports.changePassword = async (userId, oldPassword, newPassword) => {
  const user = await this.getProfile(userId);
  await this.passwordCompare(user._id, oldPassword);
  await User.findOneAndUpdate({
    '_id': userId,
  }, {
    'password': await bcrypt.hash(newPassword, 10),
  });
};

module.exports.deleteProfile = async (userId) => {
  await User.deleteOne({'_id': userId});
  await Note.deleteMany({'userId': userId});
};

module.exports.passwordCompare = async (userId, comparPassword) => {
  const user = await this.getProfile(userId);
  if (!await bcrypt.compare(comparPassword, user.password)) {
    throw new PasswordErrors('Wrong password');
  };
};
