const express = require('express');
const {Router} = express;
const router = new Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('./middlewares/validationMiddeleware');
const {login, registration} = require('../controlers/authController');

router.post('/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(login));

module.exports = router;
